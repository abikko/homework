package com.company;/* package whatever; // don't place package name! */

import java.util.*;
import java.lang.*;
import java.io.*;
import java.util.concurrent.TimeUnit;

/* Name of the class has to be "Main" only if the class is public. */
class Main
{
    private static int mCount = 0;
    public static void main (String[] args)
    {
        for(int x=0;x<10;x++){
            new Thread(new Runnable(){
                @Override
                public void run(){
                    for(int y=0;y<100;y++){
                        mCount++;
                        System.out.println(mCount+" "+Thread.currentThread().getName());
                        try{
                            TimeUnit.MILLISECONDS.sleep(100);
                        } catch(InterruptedException e){
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
        }
    }
}