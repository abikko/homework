package com.company;

abstract public class Discount {
    abstract String getDiscount();
}
