package com.company;

public class CookBookDiscount extends Discount {

    @Override
    String getDiscount() {
        String discount = "30% between Dec 1 and 24";

        return discount;
    }
}
